/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

if (typeof(Number.prototype.toDeg) === "undefined") {
  Number.prototype.toDeg = function() {
    return this * 180 / Math.PI;
  }
}

var MIN_LAT = (-90).toRad();
var MAX_LAT =  (90).toRad();
var MIN_LON = (-180).toRad();
var MAX_LON = (180).toRad();

function hrdiff(t1, t2) {
    var s = t2[0] - t1[0];
    var mms = t2[1] - t1[1];
    return s*1e9 + mms;
}

function arrayFind(arr, item) {
    for (var i = 0; i < arr.length; i++) {
       if (arr[i] === item) {
          return true;
      }
  }
}

//http://stackoverflow.com/questions/1885557/simplest-code-for-array-intersection-in-javascript
exports.intersection_destructive = function (a, b)
{
  var result = new Array();
  while( a.length > 0 && b.length > 0 )
  {  
     if      (a[0] < b[0] ){ a.shift(); }
     else if (a[0] > b[0] ){ b.shift(); }
     else /* they're equal */
     {
       result.push(a.shift());
       b.shift();
     }
  }

  return result;
}

// this is too slow with large arrays
exports.array_intersection = function(arr1, arr2) {
    var t1 = process.hrtime();

    var target = arr1;
    var d = arr2;
    var result = [];
    var j = 0;
    // For faster results loop over the smaller array
    if(target.length > arr2.length) {
        target = arr2;
        d = arr1;
    }

    for (var i=0; i < target.length; ++i)
        if (d.indexOf(target[i]) != -1)
        //if(arrayFind(d, target[i]))
            result[j++] = target[i];

    var t2 = process.hrtime();
    console.log(hrdiff(t1, t2));

    return result;
}

exports.haversineDistance = function(lat1, lon1, lat2, lon2) {
    //http://www.movable-type.co.uk/scripts/latlong.html
    var R = 6371; // km
    var dLat = (lat2-lat1).toRad();
    var dLon = (lon2-lon1).toRad();
    var lat1 = lat1.toRad();
    var lat2 = lat2.toRad();

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return R * c;
}

exports.distance = function(lat1, lon1, lat2, lon2) {
    var R = 6371; // km

    var lat1rad = lat1.toRad();
    var lat2rad = lat2.toRad();
    var dLon = (lon1 - lon2).toRad();

    return Math.acos(Math.sin(lat1rad) * Math.sin(lat2rad) +
           Math.cos(lat1rad) * Math.cos(lat2rad) *
           Math.cos(dLon)) * R; 
    /*
    public double distanceTo(GeoLocation location, double radius) {
        return Math.acos(Math.sin(radLat) * Math.sin(location.radLat) +
                Math.cos(radLat) * Math.cos(location.radLat) *
                Math.cos(radLon - location.radLon)) * radius;
    }
    */
}

// http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
exports.boundingCoordinates = function(lat, lon, distance) {
    var R = 6371; // km

    // angular distance in radians on a great circle
    var radDist = distance / R;
    var latrad = lat.toRad();
    var lonrad = lon.toRad();

    var minLat = latrad - radDist;
    var maxLat = latrad + radDist;

    var minLon;
    var maxLon;

    if (minLat > MIN_LAT && maxLat < MAX_LAT) {
        var deltaLon = Math.asin(Math.sin(radDist) / Math.cos(latrad));
        minLon = lonrad - deltaLon;
        if (minLon < MIN_LON) minLon += 2 * Math.PI;
        maxLon = lonrad + deltaLon;
        if (maxLon > MAX_LON) maxLon -= 2 * Math.PI;
    } else {
        // a pole is within the distance
        minLat = Math.max(minLat, MIN_LAT);
        maxLat = Math.min(maxLat, MAX_LAT);
        minLon = MIN_LON;
        maxLon = MAX_LON;
    }

    return [ minLat.toDeg(), minLon.toDeg(), maxLat.toDeg(), maxLon.toDeg()];

/*
        double radDist = distance / radius;

        double minLat = radLat - radDist;
        double maxLat = radLat + radDist;

        double minLon, maxLon;
        if (minLat > MIN_LAT && maxLat < MAX_LAT) {
            double deltaLon = Math.asin(Math.sin(radDist) /
                Math.cos(radLat));
            minLon = radLon - deltaLon;
            if (minLon < MIN_LON) minLon += 2d * Math.PI;
            maxLon = radLon + deltaLon;
            if (maxLon > MAX_LON) maxLon -= 2d * Math.PI;
        } else {
            // a pole is within the distance
            minLat = Math.max(minLat, MIN_LAT);
            maxLat = Math.min(maxLat, MAX_LAT);
            minLon = MIN_LON;
            maxLon = MAX_LON;
        }


        return new GeoLocation[]{fromRadians(minLat, minLon),
                fromRadians(maxLat, maxLon)};
*/

}

exports.quadrant = function(lat, lon) {
    return Math.floor(lat) + '/' + Math.floor(lon);
}


