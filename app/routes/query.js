var redis = require('redis').createClient();
var util = require('../util');
var url = require('url');

exports.query = function(req, res){
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;

    var lat = parseFloat(query.lat) || 59.0;
    var lon = parseFloat(query.lon) || 59.0;
    var distance = parseInt(query.distance) || 100;//km

    //console.log('Lat:', lat, 'Lon:', lon, 'Distance:', distance);

    var bounds = util.boundingCoordinates(lat,lon,distance);
    //console.log('Bounds:', bounds);

    redis.zrangebyscore('objects_by_lat', bounds[0], bounds[2], function(err, resultslat) {
        redis.zrangebyscore('objects_by_lon', bounds[1], bounds[3], function(err, resultslon) {
            resultslon.sort();
            resultslat.sort();
            //console.log('Sort');

            //var results = util.array_intersection(resultslat, resultslon);
            //console.log('results:', results.length);

            var results2 = util.intersection_destructive(resultslat, resultslon);

            redis.mget(results2, function(err, items) {
                //console.log('Redis results: ', items.length);
                //console.log('Item 1', items[0]);
                //res.render('query', { results: items });
                res.send('<pre>' + 'Results: ' + items.length + '\n' + items.join('\n') + '</pre>');
            });
        });
    });
};

exports.querylua = function(req, res){
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;

    var lat = parseFloat(query.lat) || 59.0;
    var lon = parseFloat(query.lon) || 59.0;
    var distance = parseInt(query.distance) || 100;//km

    //console.log('Lat:', lat, 'Lon:', lon, 'Distance:', distance);

    var bounds = util.boundingCoordinates(lat,lon,distance);
    //console.log('Bounds:', bounds);

    redis.evalsha('51048c37d9352eae25bd81e45169a6e47c97653d', 2, 'objects_by_lat', 'objects_by_lon', bounds[0], bounds[1], bounds[2], bounds[3], function(err, results) {
        //console.log('Results', results);
        redis.mget(results, function(err, items) {
            res.send('<pre>' + 'Results: ' + items.length + '\n' + items.join('\n') + '</pre>');
        });
    });
};
