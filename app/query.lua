--redis.log(redis.LOG_WARNING, '#start')

local latset = KEYS[1]
local lonset = KEYS[2]

local lat1 = ARGV[1]
local lon1 = ARGV[2]
local lat2 = ARGV[3]
local lon2 = ARGV[4]

local results = {}

local lat = redis.call('zrangebyscore', latset, lat1, lat2)
--redis.log(redis.LOG_WARNING, '#query1')

local lon = redis.call('zrangebyscore', lonset, lon1, lon2) 
--redis.log(redis.LOG_WARNING, '#query2')

local function find(a, tbl)
    for _,a_ in ipairs(tbl) do 
        if a_ == a then return true end 
    end
end

local function intersection(a, b)
    local res = {}
    for _,b_ in ipairs(b) do
        if find(b_, a) then 
            table.insert(res, b_) 
        end
    end
    return res
end

--http://stackoverflow.com/questions/1885557/simplest-code-for-array-intersection-in-javascript
local function intersection_destructive(a, b)
    -- input must be sorted
    local res = {}
    while( #a > 0 and #b > 0 ) do
        if 
            a[1] < b[1] then table.remove(a, 1)
        elseif 
            a[1] > b[1] then table.remove(b, 1)
        else
            table.insert(res, a[1])
            table.remove(a,1)
            table.remove(b,1)
        end
    end
    return res
end

local function intersection2(a, b)
    -- input must be sorted
    local res = {}

    local indexa, indexb = 1,1
    
    while #a > indexa and #b > indexb  do
        if a[indexa] < b[indexb] then 
            indexa = indexa + 1
        elseif a[indexa] > b[indexb] then 
            indexb = indexb + 1
        else
            table.insert(res, a[indexa])
            indexa = indexa + 1
            indexb = indexb + 1            
        end
    end
    return res
end

--redis.log(redis.LOG_WARNING, 'Query: ' .. lat1 .. ', ' .. lon1 .. ' / ' .. lat2 .. ', ' .. lon2 )
--redis.log(redis.LOG_WARNING, 'Lat: ' .. #lat)
--redis.log(redis.LOG_WARNING, 'Lon: ' .. #lon)

--lat={'object:6', 'object:9', 'object:1','object:21','object:2','object:3','object:4'}
--lon={'object:3','object:4','object:5', 'object:21', 'object:22'}

table.sort(lat)
--table.sort(lat, function(a,b) return a < b end)
--redis.log(redis.LOG_WARNING, '#sort1')

table.sort(lon)
--table.sort(lon, function(a,b) return a < b end)
--redis.log(redis.LOG_WARNING, '#sort2')

--redis.log(redis.LOG_WARNING, '#lat' .. table.concat(lat, ','))
--redis.log(redis.LOG_WARNING, '#lon' .. table.concat(lon, ','))

--local result = intersection(lat, lon)
--redis.log(redis.LOG_WARNING, '#intersect2')

--local result = intersection_destructive(lat, lon)
--redis.log(redis.LOG_WARNING, '#intersect1')

local result = intersection2(lat, lon)
--redis.log(redis.LOG_WARNING, '#intersect2')

-- todo get values to avoid a second redis query

return result

