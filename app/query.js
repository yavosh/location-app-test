var util = require('./util');


console.log("Distance[H]: 59,59.1 to 59,59.2 ", util.haversineDistance(59,59.1,59,59.2));
console.log("Distance: 59,59.1 to 59,59.2 ", util.distance(59,59.1,59,59.2));

console.log("Bounds: 59,59 57km ", util.boundingCoordinates(59,59,5.7));


console.log("Quadrant: ", util.quadrant(59.2, 62.2));


a=[1,2,3,4];
b=[3,4,5];

a.sort();
b.sort();

console.log("Array intersection: ", util.array_intersection(a,b));
console.log("Array intersection2: ", util.array_intersection(a,b));