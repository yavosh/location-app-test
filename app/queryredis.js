
var redis = require('redis').createClient();
var util = require('./util');

var lat = 59.0;
var lon = 59.0;
var distance = 50;//km

var bounds = util.boundingCoordinates(lat,lon,distance);

console.log('Bounds:', bounds);

redis.zrangebyscore('objects_by_lat', bounds[0], bounds[2], function(err, resultslat) {
    redis.zrangebyscore('objects_by_lon', bounds[1], bounds[3], function(err, resultslon) {
        console.log('resultslat:', resultslat.length);
        console.log('resultslon:', resultslon.length);

        resultslon.sort();
        resultslat.sort();
        console.log('Sort');

        var results2 = util.intersection_destructive(resultslat, resultslon);
        console.log('results2:', results2.length);

        redis.mget(results2, function(err, items) {
            console.log('Redis results: ', items.length);
            console.log('Item 1', items[0]);
        });

        console.log('End closing redis')
        redis.quit();
    });
});