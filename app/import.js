var fs = require('fs');
var lazy = require('lazy');
var redis = require('redis').createClient();

redis.on("error", function (err) {
        console.log("Error " + err);
});

redis.select(0);

var count = 0;
var countCities = 0;

redis.del('objects', redis.print);
redis.del('objects_by_lat', redis.print);
redis.del('objects_by_lon', redis.print);

var printRedisError = function(err, result) {
    if(err) console.log("Error: ", err);
}

var stream = fs.createReadStream('../data/RU.txt', {'options': 'r'});
var reader = new lazy(stream)
    .lines
    //.take(5)
    .forEach(function(line) {
        var dataLine = line.toString();
        var data = dataLine.split('\t');
        count++;

        // We only want cities
        if(data[6] == 'P') {
            countCities++;

            if(data[2] == 'Moscow') {
                console.log('object:'+data[0], data[2] + '|' + data[4] + '|' + data[5] + '|' + data[6]);
            }

            redis.set('object:'+data[0], data[2] + '|' + data[4] + '|' + data[5] + '|' + data[6], printRedisError);
            redis.zadd('objects_by_lat', data[4], 'object:' + data[0], printRedisError);
            redis.zadd('objects_by_lon', data[5], 'object:' + data[0], printRedisError);
        }
    });


stream.on('end', function() { 
    console.log('Finished...');
    console.log('Counters Objects:', count, ' Cities:', countCities);
    redis.quit();
});
